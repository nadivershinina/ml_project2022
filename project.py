from random import randint
from time import time as time
import csv

def read_data(filename):
    with open(filename, 'r') as book_file:
        book = book_file.readlines()

    return book

def make_1mb(raw_file, file_1mb):
    n_lines = len(raw_file)

    with open(file_1mb, 'w') as hfile:
        for i in range(lines_for_1_mb):
            hfile.write(raw_file[randint(0, n_lines - 1)])

def get_name_without_extension(filename):
    return filename[:filename.rfind(".")]

def make_big(raw_data_from_file_1mb, file_name_1mb):
    for file_size in file_sizes_inter:
        start = time()
        for i in range(file_sizes_inter[file_size]):
            with open(f'{get_name_without_extension(file_name_1mb)}_{file_size}.txt', 'a') as hfile:
                hfile.writelines(raw_data_from_file_1mb)

        print(f'To write {file_size} we need {time() - start} seconds.')

def make_big_files(filename, filename_1mb):
    raw_data = read_data(filename)
    print(f'Number of lines in file: {len(raw_data)}')
    make_1mb(raw_data, filename_1mb)
    raw_data_1mb = read_data(filename_1mb)
    make_bigger(raw_data_1mb, filename_1mb)

if __name__ == '__main__':
    start_make_big_files(book, book_1MB)
